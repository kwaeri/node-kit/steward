/**
 * SPDX-PackageName: kwaeri/steward
 * SPDX-PackageVersion: 0.5.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import {
    KWAERI,
    NodeKitOptions
} from '@kwaeri/standards-types';
import { Steward } from '../src/steward.mjs';
import * as _fs from 'fs/promises';
import * as assert from 'assert';
import debug from 'debug';


// DEFINES
let STEWARD_ENV = process.env.NODE_ENV;
if( !STEWARD_ENV || STEWARD_ENV === ( "" || null ) ) {
    STEWARD_ENV = "default";
}

const steward = new Steward( STEWARD_ENV as KWAERI.ENV.DEFAULT | KWAERI.ENV.PRODUCTION | KWAERI.ENV.TEST | undefined ),
      DEBUG = debug( 'kue:mysql-migration-generator-test' ),
      year = new Date().getFullYear(),
      month = ( new Date().getUTCMonth() + 1 ),
      LATEST_MIGRATION_DIR = `data/migrations/${year}/${month}`,
      LATEST_MIGRATION_PARENT_DIR = `data/migrations/${year}`;

let DELETE_MIGRATION = false;

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );

    }
);


describe(
    'Steward Functionality Test Suite',
    () => {


        describe(
            'Get Service Providers Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const result = await steward.getProviders();

                        assert.equal(
                            JSON.stringify( result.list ) ,
                            JSON.stringify( [ '@kwaeri/node-kit-project-generator', '@kwaeri/mysql-migrator', '@kwaeri/mysql-migration-generator' ]  )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Contracts Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const  result = await steward.getContracts();

                        assert.equal( JSON.stringify( result.contracts ), JSON.stringify(  {"commands":{"new":{"project":true},"migrations":false,"add":{"migration":true}},"required":{"new":{"project":{"type":["api","react"]}},"migrations":{},"add":{"migration":{/*"type":["mysql","pg","mongodb"]*/}}},"optional":{"new":{"project":{"redux":{"for":"type=react","flag":true},"lang":{"for":false,"flag":false,"values":["typescript","javascript"]},"skip-wizard":{"for":false,"flag":true}}},"migrations":{"step-back":{"for":false,"flag":false,"values":null}},"add":{"migration":{"skip-wizard":{"for":false,"flag":true},"lang":{"for":false,"flag":false,"values":["typescript","javascript"]}}}}} ) );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Help Text Test',
            () => {
                it(
                    'Should return true.',
                    () => {
                        const result = steward.getHelpTexts();

                        assert.equal( JSON.stringify( result.helpTexts ), JSON.stringify(
                            //{"NodeKitProjectGenerator":{"commands":{"new":{"description":"The 'new' command automates content creation.","specifications":{"project":{"description":"Creates a new empty project of the type specified, and according to options provided.","options":{"required":{"type":{"description":"Denotes the type of the project that will be generated.","values":{"api":{"desccription":"A NodeKit based MV(A)C API project."},"react":{"description":"A NodeKit based client-side React project"}}}},"optional":{"specification":{"project":{"language":{"description":"Denotes the programming language for the project being generated.","values":["typescript","javascript"]}}},"type":{"react":{"redux":{"description":"Denotes that the project should include redux support","values":false}}}}}}}}}},"MysqlMigrator":{"commands":{"migrations":{"description":"The 'migrations' command automates the handling of migrations for an existing project.","specifications":{},"options":{"optional":{"command":{"step-back":{"description":"Specify the number of migrations to undo from those that are applied.","values":null}}}}}}}}
                            {"NodeKitProjectGenerator":{"commands":{"new":{"description":"The 'new' command automates content creation.","specifications":{"project":{"description":"Creates a new empty project of the type specified, and according to options provided.","options":{"required":{"type":{"description":"Denotes the type of the project that will be generated.","values":{"api":{"desccription":"A NodeKit based MV(A)C API project."},"react":{"description":"A NodeKit based client-side React project"}}}},"optional":{"specification":{"language":{"description":"Denotes the programming language for the project being generated.","values":["typescript","javascript"]}},"type":{"react":{"redux":{"description":"Denotes that the project should include redux support","values":false}}}}}}},"options":{"optional":{}}}}},"MysqlMigrator":{"commands":{"migrations":{"description":"The 'migrations' command automates the handling of migrations for an existing project.","specifications":{},"options":{"optional":{"command":{"step-back":{"description":"Specify the number of migrations to undo from those that are applied.","values":null}}}}}}},"MysqlMigrationGenerator":{"commands":{"add":{"description":"The 'add' command automates content creation for an existing project.","specifications":{"migration":{"description":"Adds a new empty migration of the type specified to the existing project, and according to options provided.","options":{"required":{"type":{"description":"Specify the template type to use when generating a migration for a project developed with @kwaeri/node-kit. Possible types include 'mysql', 'pg', 'mongodb'.","values":{"mysql":{"desccription":"A mysql based migration."},"pg":{"description":"A postgreSQL based migration."},"mongodb":{"description":"A mongodb based migration."}}}},"optional":{"specification":{"language":{"description":"Denotes the programming language for the migration being generated.","values":["typescript","javascript"]}}}}}},"options":{"optional":{}}}}}}
                            )
                        );
                        //assert.equal( JSON.stringify( result.helpTexts ), JSON.stringify( {"NodeKitProjectGenerator":{"commands":{"new":{"description":"The 'new' command automates content creation.","specifications":{"project":{"description":"Creates a new empty project of the type specified, and according to options provided.","options":{"required":{"type":{"description":"Denotes the type of the project that will be generated.","values":{"api":{"desccription":"A NodeKit based MV(A)C API project."},"react":{"description":"A NodeKit based client-side React project"}}}},"optional":{"specification":{"project":{"language":{"description":"Denotes the programming language for the project being generated.","values":["typescript","javascript"]}}},"type":{"react":{"redux":{"description":"Denotes that the project should include redux support","values":false}}}}}}}}}}} ) );
                    }
                )
            }
        );

        describe(
            'Render Service Test - [I. From Remote]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const options: NodeKitOptions = {
                            environment: STEWARD_ENV as KWAERI.ENV.DEFAULT | KWAERI.ENV.PRODUCTION | KWAERI.ENV.TEST | undefined,
                            quest: "new",
                            specification: "project",
                            subCommands: [ "Test Project" ],
                            args: { type: "rest" },
                            version: ""
                        };

                        const result = await steward.delegate( options );

                        if( result && ( result as any ).result )
                        await _fs.rm( "test-project", { recursive: true, force: true } );

                        assert.equal(
                            JSON.stringify( result ),
                        JSON.stringify( { specification: "project", result: true, contracts: [ { type: "create_project_from_remote", result: true } ] } )
                        );
                    }
                )
            }
        );

        describe(
            'Render Service Test - [II. from Cache]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const options: NodeKitOptions = {
                            environment: STEWARD_ENV as KWAERI.ENV.DEFAULT | KWAERI.ENV.PRODUCTION | KWAERI.ENV.TEST | undefined,
                            quest: "new",
                            specification: "project",
                            subCommands: [ "Test Project" ],
                            args: { type: "rest" },
                            version: ""
                        };

                        const result = await steward.delegate( options );

                        if( result && ( result as any ).result ) {
                            await _fs.rm( "test-project", { recursive: true, force: true } );
                            await _fs.rm( ".kue_cache", { recursive: true, force: true } );
                        }

                        assert.equal(
                            JSON.stringify( result ),
                            JSON.stringify( { specification: "project", result: true, contracts: [ { type: "create_project_from_cache", result: true } ] } )
                        );
                    }
                )
            }
        );

        describe(
            'Render Service Test - [III. Add MySQL Migration]',
            () => {
                it(
                    'Should create a MySQL migration in JavaScript.',
                    async () => {
                        const options: NodeKitOptions = {
                            environment: STEWARD_ENV as KWAERI.ENV.DEFAULT | KWAERI.ENV.PRODUCTION | KWAERI.ENV.TEST | undefined,
                            quest: "add",
                            specification: "migration",
                            subCommands: [ "AddTestTableToDatabase" ],
                            args: {},
                            version: ""
                        };

                        const result = await steward.delegate( options );

                        // Do some preemptive clean-up to ensure subsequent tests
                        const check = await _fs.access( await _fs.realpath( `./${LATEST_MIGRATION_DIR}` ), _fs.constants.F_OK );

                        // If a new year/month migration path was created, mark
                        // it for deletion
                        if( check === undefined )
                            DELETE_MIGRATION = true;

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( result ),
                                JSON.stringify( { specification: 'migration', result: true, contracts: [ { result: true, type: "add_mysql_migration" } ] } )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Render Service Test - [IV. Migrations Applied]',
            () => {
                it(
                    'Should return true, indicating that the test migration was applied.',
                    async () => {
                        const options: NodeKitOptions = {
                            environment: STEWARD_ENV as KWAERI.ENV.DEFAULT | KWAERI.ENV.PRODUCTION | KWAERI.ENV.TEST | undefined,
                            quest: "migrations",
                            specification: "",
                            subCommands: [],
                            args: {},
                            version: ""
                        };

                        const result = await steward.delegate( options );

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( result ),
                                JSON.stringify( { specification: '', result: true, contracts: [ { result: true, type: "run_migrations" } ] } )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Render Service Test - [V. Migrations Reverted]',
            () => {
                it(
                    'Should return true, indicating that the applied test migration was reverted.',
                    async () => {
                        const options: NodeKitOptions = {
                            environment: STEWARD_ENV as KWAERI.ENV.DEFAULT | KWAERI.ENV.PRODUCTION | KWAERI.ENV.TEST | undefined,
                            quest: "migrations",
                            specification: "",
                            subCommands: [],
                            args: { "step-back": "2" },
                            version: ""
                        };

                        const result = await steward.delegate( options );

                        // Do some preemptive clean-up to ensure subsequent tests
                        if( DELETE_MIGRATION === true )
                            await _fs.rm( LATEST_MIGRATION_PARENT_DIR, { recursive: true, force: true } );

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( result ),
                                JSON.stringify( { specification: '', result: true, contracts: [ { result: true, type: "run_migrations" } ] } )
                            )
                        );
                    }
                );
            }
        );


    }
);